<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Materializecss.CardGame</title>

  <!-- CSS  -->
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="fonts/font-awesome/css/fontawesome-all.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="shortcut icon" href="img/logo.png">
  
</head>
<body class="white">
    
    <div class="container">
        
        <div class="row">
        	<div class="col s12 m12">
            	<h3 class="light title center-align">Materializecss.CardGame</h3>
            </div>
            
            
            <div class="col s12 m4">
                <div class="col s12 m12">
                    <p class="left-align">
                        Clique no baralho para sacar uma carta.
                    </p>
                    <div class="col s6 m4 l4 " id="DeckOverTableHolder">
                    	
                        <div class="waves waves-effect  col s12 z-depth-2 blue darken-2 hoverable card-padding " id="DeckHasCards">
                            <p class="center-align blue white-text ">?</p>
                        </div>
                        <div class="waves waves-effect  col s12 z-depth-2 white hoverable card-padding hide" id="DeckHasNoCards">
                            <p class="center-align white black-text "><i class="fas fa-ban"></i></p>
                        </div>
                    </div>
                    <p class="left-align">
                    	Cartas restantes: <strong id="cardsLeft">52</strong>
                    </p>
                </div>
            </div>
            <div class="col s12 m8">
                
                <div class="col s12 m6">
                    <h5 class="light  left-align">Seus pontos</h5>
                    <h4 class="light  left-align" id="playerPoints">0</h4>
                </div>
                
                <div class="col s12 m6" id="">
                    <h5 class="light  right-align">Pontos da mesa</h5>
                    <h4 class="light  right-align" id="tablePoints">0</h4>
                </div>
                <div class="col s12 m12 center-align z-depth-4 hide" id="victoryHolder">
                    <h5 class="light  center-align" id="playerVictoryDefinerHolder">Jogador <span id="playerVictoryDefiner"></span>!</h5>
                    <h5 class="light  center-align" id="tableVictoryDefinerHolder">Mesa <span id="tableVictoryDefiner"></span>!</h5>
                    <a href="" class="waves-effect waves-light btn"><i class="fas fa-sync-alt left"></i>Jogar novamente</a>
                    <br><br>

                </div>
                <div class="col s12 m12">
                	<br>
                </div>
                <div class="col s12 m6 z-depth-1">
                    <h5 class="light  left-align">Sua mão</h5>
                    <div class="left-align" id="playerHand"></div>
                    
                </div>
                <div class="col s12 m6 z-depth-1">
                    <h5 class="light  right-align">Mão da mesa</h5>
                    <div class="right-align" id="tableHand"></div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
	<div class="container">
        <div class="row">
            <div class="col s12 m12">
                <div class="row" id="DeckTable">
                    
                    
                </div>
            </div>
        </div>
    </div>


  <!--  Scripts-->
  <script src="js/jquery.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script src="js/cardgame.classes.js"></script>
	<script src="js/index.js"></script>
  </body>
</html>
