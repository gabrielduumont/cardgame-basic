

var CardGameLib = (function() {
	return {
		
		CardColor: function(){
			var colors = ['Clubs','Diamonds','Spades','Hearts'];	
			
			this.Get = function(i){
				return colors[i];
			};
		},
		
		
		Card: function (v, c){
			var value = v;
			var face;
			var color = c;
			var htmlObject;
			
			if(value === 1){
				face = 'A';
			}
			else if(v === 11){
				face = 'J';
			}
			else if(v === 12){
				face = 'Q';
			}
			else if(v === 13){
				face = 'K';
			}
			else{
				face = v;
			}
			
			
			this.Set = function(va, co){
				value = va;
				if(value === 1){
					face = 'A';
				}
				else if(value === 11){
					face = 'J';
				}
				else if(value === 12){
					face = 'Q';
				}
				else if(value === 13){
					face = 'K';
				}
				else{
					face = value;
				}
				
				color = co;
			};
			
			this.SetValue = function(va){
				value = va;
			};
			
			this.SetFace = function(fa){
				face = fa;
			};
			
			this.SetColor = function(co){
				color = co;
			};
			
			
			this.Print = function(){
				console.log(value+' '+face+' '+color);
				
			};
			
			this.GetValue = function(){
				return value;
			};
			
			this.GetFace = function(){
				return face;
			};
			this.GetColor = function(){
				return color;
			};
			
			
			this.SetHtmlElement = function(id){
				
				var cardWrapper_Element = document.createElement("div");
				var cardPadding_Element = document.createElement("div");
				var cardPadding_P_Element = document.createElement("p");
				var cardPadding_img_Element = document.createElement("img");
				cardWrapper_Element.className = 'col s3 m2 l1 ';
				cardWrapper_Element.id = 'cardHolder_'+id;
				cardWrapper_Element.dataset.position = id;
				cardWrapper_Element.draggable = true;
				cardWrapper_Element.ondragstart ="drag(event)";
				cardPadding_Element.className = 'col s12 z-depth-2 white hoverable card-padding';
				cardPadding_P_Element.className = 'center-align white';
				cardPadding_P_Element.innerHTML = face;
				cardPadding_img_Element.className = 'left';
				cardPadding_img_Element.src = 'img/'+color+'.png';
				cardPadding_img_Element.width = '15';
				
				cardPadding_Element.appendChild(cardPadding_img_Element);
				cardPadding_Element.appendChild(cardPadding_P_Element);
				cardWrapper_Element.appendChild(cardPadding_Element);
				
				htmlObject = cardWrapper_Element;
				//console.log(htmlObject);
				//window.setTimeout(function(){
					document.getElementById("DeckTable").appendChild(htmlObject);	
				//},1500);
			};
			
			
			
			this.SetElementOnHand = function(id, idhand){
				
				var cardWrapper_Element = document.createElement("div");
				var cardPadding_Element = document.createElement("div");
				var cardPadding_P_Element = document.createElement("p");
				var cardPadding_img_Element = document.createElement("img");
				cardWrapper_Element.className = 'col s6 m4 l4 ';
				cardWrapper_Element.id = 'cardHolder_'+id;
				cardWrapper_Element.dataset.position = id;
				cardWrapper_Element.draggable = true;
				cardWrapper_Element.ondragstart ="drag(event)";
				cardPadding_Element.className = 'col s12 z-depth-2 white hoverable card-padding';
				cardPadding_P_Element.className = 'center-align white';
				cardPadding_P_Element.innerHTML = face;
				cardPadding_img_Element.className = 'left';
				cardPadding_img_Element.src = 'img/'+color+'.png';
				cardPadding_img_Element.width = '15';
				
				cardPadding_Element.appendChild(cardPadding_img_Element);
				cardPadding_Element.appendChild(cardPadding_P_Element);
				cardWrapper_Element.appendChild(cardPadding_Element);
				
				htmlObject = cardWrapper_Element;
				//console.log(htmlObject);
				//window.setTimeout(function(){
					document.getElementById(idhand).appendChild(htmlObject);	
				//},1500);
			};
			
			
			this.Compare = function(va){
				value = va;
			};
			
			
		},
		
		
		
		
		
		
		Deck: function (){
			var cards = new Array();
			
			this.Get = function(){
				return cards;
			};
			
			this.Log = function(){
				return console.log(cards);
			};
			
			this.Print = function(){
				var i;
				console.log('-----PRINTING DECK-------');
				var counter = 0;
				for (i in cards) {
					counter++;
					cards[i].Print();
					
					
				}
				console.log('-----FINISHED PRINTING DECK-------');
			};
			
			this.SetTable = function(){
				var i;
				var counter = 0;
				for (i in cards) {
					counter++;
					//cards[i].Print();
					//window.setTimeout(function(){
						cards[i].SetHtmlElement(counter);
					//},20);
					
				}
			};
			
			this.Shuffle = function(){
				cards.sort(function(a, b){return 0.5 - Math.random()});
			};
			
			this.Init = function(){
				cards= new Array();
				for(c = 0; c<4;++c){
					for(i =1; i<14; ++i){
						var color = new CardGameLib.CardColor();
						var newCard = new CardGameLib.Card(i, color.Get(c));
						cards.push(newCard);
						
					}
				}
				
			}();
			
			
			this.DrawCard = function(){
				var cardReturn = cards.splice(cards.length-1,1)[0];
				var totalCardsLeft = parseInt($('#cardsLeft').html())-1;
				$('#cardsLeft').html(totalCardsLeft);
				return cardReturn;
			};
			
			
			this.EndGame = function(playerA, playerB){
				if(playerA.GetPoints() === playerB.GetPoints()){
					$('#playerVictoryDefinerHolder').addClass('orange-text');
					$('#tableVictoryDefinerHolder').addClass('orange-text');
					
					$('#playerVictoryDefiner').html('empatou');
					$('#tableVictoryDefiner').html('empatou');
				}
				else if(playerA.GetPoints() < playerB.GetPoints()){
					$('#playerVictoryDefinerHolder').addClass('red-text');
					$('#tableVictoryDefinerHolder').addClass('green-text');
					
					$('#playerVictoryDefiner').html('perdeu');
					$('#tableVictoryDefiner').html('ganhou');
				}
				else {
					$('#playerVictoryDefinerHolder').addClass('green-text');
					$('#tableVictoryDefinerHolder').addClass('red-text');
					
					$('#playerVictoryDefiner').html('ganhou');
					$('#tableVictoryDefiner').html('perdeu');
				}
				$('#victoryHolder').hide(150,function(){
					$('#victoryHolder').removeClass('hide');
					$('#victoryHolder').show(150,function(){
						
					});
				});
				
			};
		},
		
		
		
		
		Player: function (hh, ph){
			var hand = new Array();
			var points = 0;
			var handHolder = hh;
			var pointsHolder = ph;
			
			this.SetCardOnHand = function(card, idcard){
				hand.push(card);
				points += card.GetValue();
				card.SetElementOnHand(idcard, handHolder);
				document.getElementById(pointsHolder).innerHTML = points;
			};
			
			this.Log = function(){
				return console.log(this);
			};
			
			
			this.GetPoints = function(){
				return points;
			};
			
			
			
		},
		
		Game: function (){
			
			this.Run = function(){
				console.log('---------------------------------');	
				console.log('it starts...');
				var cardsPickerCounter = 1;
				var roundCounter = 0;
				var Deck = new CardGameLib.Deck();
				var Player1 = new CardGameLib.Player('playerHand','playerPoints');
				var Player2 = new CardGameLib.Player('tableHand','tablePoints');
				//console.log(Deck.Get());
				//Deck.Print();
				Deck.Shuffle();
				//Deck.Print();
				//Deck.SetTable();
				
				$('#DeckOverTableHolder').click(function(e) {
					e.preventDefault();
					roundCounter++;
					console.log('round '+roundCounter+' start..');
					if(cardsPickerCounter > 52){
						return alert('Fim das cartas');
					}
					else{
						Player1.SetCardOnHand(Deck.DrawCard(), cardsPickerCounter);
						cardsPickerCounter++;
						Player2.SetCardOnHand(Deck.DrawCard(), cardsPickerCounter);
						cardsPickerCounter++;
						console.log('round '+roundCounter+' end..');
						if(cardsPickerCounter > 51){
							$('#DeckHasCards').addClass('hide');	
							$('#DeckHasNoCards').removeClass('hide');	
							Deck.EndGame(Player1, Player2);
							console.log('it ends...');
							console.log('---------------------------------');
						}
					}
					
				});
			};
			
		}
		
		
		
		
		
		
		
		
		
		
	}
      
})();

